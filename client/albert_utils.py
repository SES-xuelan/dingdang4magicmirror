# -*- coding: utf-8-*-
from __future__ import print_function

# Albert的语音处理
# 返回是否处理过了
def handle_voice_text(text):

    if (u'谁' in text) and (u'最美' in text):
        #魔镜魔镜告诉我，谁是世界上最美丽的人
        with open('/var/www/html/magicmirror/stt/stt.txt', 'w') as f:
            f.write(u'1')
        return True
    elif u'你好' in text:
        #你好
        with open('/var/www/html/magicmirror/stt/stt.txt', 'w') as f:
            f.write(u'2')
        return True

    return False
